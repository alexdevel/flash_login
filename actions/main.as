﻿// create a custom package
package actions
{
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.*;
	import flash.text.*;
	
	public class main extends MovieClip
	{
			public function main()
			{
				trace("test constructor");
				
				submit_button.buttonMode = true;

			

			submit_button.addEventListener(MouseEvent.MOUSE_DOWN, checkLogin);
			
			
			username.text = "";
			password.text = "";
				
			}
			
			
			public function showResult (event:Event):void {
 
    /*
 
    this autosizes the text field
 
    ***** You will need to import flash's text classes. You can do this by adding: 
 
    import flash.text.*;
 
    ...to your list of import statements 
 
    */
 
    result_text.autoSize = TextFieldAutoSize.LEFT;
 
    /*
    this gets the output and displays it in the result text field
    */
 
    result_text.text = "" + event.target.data.systemResult;
 
}
			
			
			public function processLogin ():void {
			
			/*
			variables that we send to the php file
			*/
		
			var phpVars:URLVariables = new URLVariables();
			
			/*
			we create a URLRequest  variable. This gets the php file path.
			*/
			
			var phpFileRequest:URLRequest = new URLRequest("php/controlpanel.php");
			
			/*
			this allows us to use the post function in php
			*/
			
			phpFileRequest.method = URLRequestMethod.POST;
			
			/*
			attach the php variables to the URLRequest
			*/
			
			phpFileRequest.data = phpVars;
			
			/*
			create a new loader to load and send our urlrequest
			*/
			
			var phpLoader:URLLoader = new URLLoader();
			phpLoader.dataFormat = URLLoaderDataFormat.VARIABLES;			
			phpLoader.addEventListener(Event.COMPLETE, showResult);
			
			/*
			now lets create the variables to send to the php file
			*/
			
			phpVars.systemCall = "checkLogin";
			phpVars.username = username.text;
			phpVars.password = password.text;
			
			/*
			this will start the communication between flash and php
			*/
			
			phpLoader.load(phpFileRequest);
		
		}
			
			
			public function checkLogin(MouseEvent)
			{
			trace("submit button");
			
					if(username.text == "" || password.text == "")
					{
					
					
						if(username.text == "")
						{
						username.text = "enter username";
						}
						
						if(password.text == "")
						{
						password.text = "enter password";
						}
					
					}
					else
					{
					processLogin();
					}
			
			}
		
	}
	
	
}